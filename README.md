## Name
Intro to MLOps on AWS

## Description
This is a code repository for MLOps Engineering on the Amazon Sagemaker Service

## Purpose
Builds, Trains, and Registers a SageMaker Model