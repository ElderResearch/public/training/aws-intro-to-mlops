"""Example workflow pipeline script for wine quality pipeline.
    
                                                    Create and Register Model
                                                .
    Preprocess-> Train-> Evaluate-> Condition .
                                                .
                                                    (stop)
 """
#!/usr/bin/env python
import os
import json
import boto3
import sagemaker
import sagemaker.session
import logging

from sagemaker.workflow.parameters import (
    ParameterInteger,
    ParameterString,
)

from sagemaker.processing import (
    ProcessingInput,
    ProcessingOutput,
    ScriptProcessor,
)
from sagemaker.sklearn.processing import SKLearnProcessor
from sagemaker.workflow.steps import (
    ProcessingStep,
    TrainingStep
)
from datetime import datetime
from sagemaker.workflow.pipeline import Pipeline
from sagemaker.estimator import Estimator
from sagemaker.inputs import TrainingInput
from sagemaker.workflow.properties import PropertyFile
from sagemaker.model import Model
from sagemaker.inputs import CreateModelInput
from sagemaker.workflow.conditions import ConditionLessThanOrEqualTo
from sagemaker.workflow.condition_step import ConditionStep
from sagemaker.workflow.functions import JsonGet
from sagemaker.workflow.steps import CreateModelStep
from sagemaker.model_metrics import MetricsSource, ModelMetrics 
from sagemaker.workflow.step_collections import RegisterModel

logger = logging.getLogger()
logger.setLevel(logging.INFO)
logger.addHandler(logging.StreamHandler())
# stop propagting to root logger
#logger.propagate = False


################################
# Set up SageMaker Environment #
################################

logger.info("Setting up SageMaker environment.")
region = boto3.Session().region_name
sagemaker_session = sagemaker.session.Session()
role = sagemaker.get_execution_role()
default_bucket = sagemaker_session.default_bucket()
model_package_group_name = f"WineModelPackageGroup" 

################################
# Data-Set Preparation #
################################

# Download Dataset dataset into accounts default Amazon S3 Bucket
base_uri = f"s3://{default_bucket}/wine_data"
logger.info("Downloading datasets into S3 Bucket: %s.", base_uri)

today = datetime.today().strftime('%Y%m%d')

# input data
try:
    local_path = "data/input"
    input_data_uri = sagemaker.s3.S3Uploader.upload(
        local_path=local_path, 
        desired_s3_uri=f"{base_uri}/training_data",
    )
    logger.info("Succesfully uploaded input dataset to S3 Bucket.")
    logger.info("S3 Location: %s.", input_data_uri)
except Exception as e:
    logger.error("Error uploading input dataset to S3: %s.", e)


# inference data
try:
    local_path = "data/inference"
    inference_data_uri = sagemaker.s3.S3Uploader.upload(
        local_path=local_path, 
        desired_s3_uri=f"{base_uri}/inference_data_{today}",
    )
    logger.info("Successfully uploaded inference data to S3 Bucket.")
    logger.info("S3 Location: %s.", inference_data_uri)
except Exception as e:
    logger.error("Error uploading test data to S3: %s.", e)





#################################
# SageMaker Pipeline Parameters #
#################################

logger.info("Setting up pipeline parameters.")
# Enabling Checkpointing for Model Training
checkpoint_local_path="/opt/ml/checkpoints"
base_job_name="sagemaker-training-checkpoint"
checkpoint_in_bucket="checkpoints"
checkpoint_s3_bucket="s3://{}/{}/{}".format(default_bucket, base_job_name, checkpoint_in_bucket)


instance_count = ParameterInteger( 
    #instance count of the processing job
    name="ProcessingInstanceCount",
    default_value=1
)
processing_instance_type = ParameterString( 
    #instance type of the processing jobs
    name="ProcessingInstanceType",
    default_value="ml.m5.large"        #might need to be larger
)
training_instance_type = ParameterString(
    #instance type of the training jobs
    name="TrainingInstanceType",
    default_value="ml.m5.large"        #might need to be larger
)
model_approval_status = ParameterString(
    #approval status to register trained model with for CI/CD
    name="ModelApprovalStatus",
    default_value="Approved"
    #default_value="PendingManualApproval" # model will be registered but not deployed
)
input_data = ParameterString(
    #S3 location of the input data for model training 
    name="InputData",
    default_value=input_data_uri,
)
inference_data = ParameterString(
    #S3 location of the input data for batch transformation
    name="InferenceData",
    default_value=inference_data_uri,
)





#################
# Add job steps #
#################

## --- Step 1: Feature Engineering Step --- ##

proc_feature_eng = SKLearnProcessor(
    framework_version='0.23-1',
    role=role,
    instance_type=processing_instance_type,
    instance_count=instance_count,
    base_job_name="sklearn-feature-eng"
)

step_feature_engineering = ProcessingStep(
    name="WineFeatureEng",
    processor=proc_feature_eng,
    inputs=[
        ProcessingInput(source=input_data, destination="/opt/ml/processing/input"),  
    ],
    outputs=[
        ProcessingOutput(output_name="train", source="/opt/ml/processing/train"),
        ProcessingOutput(output_name="validation", source="/opt/ml/processing/validation"),
        ProcessingOutput(output_name="test", source="/opt/ml/processing/test")
    ],
    code="scripts/preprocessing.py",
)

## --- Step 2: Training Step --- ##


# Defining the location models should be saved.
model_path = f"s3://{default_bucket}/WineModel/{today}"

image_uri = sagemaker.image_uris.retrieve(
    framework="xgboost",
    region=region,
    version="1.0-1",
    py_version="py3",
)
xgb_train = Estimator(
    image_uri=image_uri,
    instance_type=training_instance_type,
    instance_count=instance_count,
    output_path=model_path,
    role=role,
    
    # Parameters required to enable checkpointing
    checkpoint_s3_uri=checkpoint_s3_bucket,
    checkpoint_local_path=checkpoint_local_path
)
xgb_train.set_hyperparameters(
    objective="reg:squarederror",
    num_round=50,
    max_depth=5,
    eta=0.2,
    gamma=4,
    min_child_weight=6,
    subsample=0.7,
    silent=0
)

step_training = TrainingStep(
    name=f"TrainWineModel",
    estimator=xgb_train,
    inputs={
        "train": TrainingInput(
            s3_data=step_feature_engineering.properties.ProcessingOutputConfig.Outputs[
                "train"
            ].S3Output.S3Uri,
            content_type="text/csv"
        ),
        "validation": TrainingInput(
            s3_data=step_feature_engineering.properties.ProcessingOutputConfig.Outputs[
                "validation"
            ].S3Output.S3Uri,
            content_type="text/csv"
        )
    },
)

## --- Step 3: Model Evaluation --- ##	

proc_eval = ScriptProcessor(	
    image_uri=image_uri,	
    command=["python3"],	
    instance_type=processing_instance_type,	
    instance_count=instance_count,	
    base_job_name="script-eval",	
    role=role,	
)	
evaluation_report = PropertyFile(	
    name="EvaluationReport",	
    output_name="evaluation",	
    path="evaluation.json"	
)	
step_evaluation = ProcessingStep(	
    name="EvalWineModel",	
    processor=proc_eval,	
    inputs=[	
        ProcessingInput(	
            source=step_training.properties.ModelArtifacts.S3ModelArtifacts,	
            destination="/opt/ml/processing/model"	
        ),	
        ProcessingInput(	
            source=step_feature_engineering.properties.ProcessingOutputConfig.Outputs[	
                "test"	
            ].S3Output.S3Uri,	
            destination="/opt/ml/processing/test"	
        )	
    ],	
    outputs=[	
        ProcessingOutput(output_name="evaluation", source="/opt/ml/processing/evaluation"),	
    ],	
    code="scripts/evaluation.py",	
    property_files=[evaluation_report],	
)	

		
## --- Step 4: Create SM Model Instance --- ##

model_path = f"s3://{default_bucket}/WineModel"
# create SageMaker model
model = Model(
    image_uri=image_uri,
    model_data=step_training.properties.ModelArtifacts.S3ModelArtifacts,
    sagemaker_session=sagemaker_session,
    role=role,
)

# define model input
inputs = CreateModelInput(
    instance_type=processing_instance_type,
    accelerator_type="ml.eia1.medium",
)

step_create_model = CreateModelStep(
    name="CreateModelInstance",
    model=model,
    inputs=inputs,
)




## --- Step 4: Register Model --- ##

# Condition Step must be satisfied 
model_metrics = ModelMetrics(
    model_statistics=MetricsSource(
        s3_uri="{}/evaluation.json".format(
            step_evaluation.arguments["ProcessingOutputConfig"]["Outputs"][0]["S3Output"]["S3Uri"]
        ),
        content_type="application/json"
    )
)
step_register_model = RegisterModel(
    name="RegisterWineModel",
    estimator=xgb_train,
    model_data=step_training.properties.ModelArtifacts.S3ModelArtifacts,
    content_types=["text/csv"],
    response_types=["text/csv"],
    inference_instances=["ml.t2.medium", "ml.m5.large"],
    transform_instances=["ml.m5.large"],
    model_package_group_name=model_package_group_name,
    approval_status=model_approval_status,
    model_metrics=model_metrics
)




## --- Condition Sub-Step for Model Accuracy --- ##

# This determines whether the model is created, and registered
# mse of current model must be less than 0.7
cond_lte = ConditionLessThanOrEqualTo(
    left=JsonGet(
        step_name=step_evaluation.name,
        property_file=evaluation_report,
        json_path="regression_metrics.mse.value"      
    ),
    right=0.7
)

# If the model passes the condition create and register the model.
step_condition = ConditionStep(
    name="WineMSECond",
    conditions=[cond_lte],
    if_steps=[
        step_create_model,
        step_register_model    
    ],
    else_steps=[], 
    
)



## --- Pipeline Instances --- ##

# pipeline instance
pipeline = Pipeline(
    name=f"WineQualityPipeline",
    parameters=[
        processing_instance_type, 
        instance_count,
        training_instance_type,
        model_approval_status,
        input_data,
        inference_data
    ],
    steps=[
        step_feature_engineering,
        step_training,
        step_evaluation,
        step_condition
    ],
    sagemaker_session=sagemaker_session
)




####################
# Run the Pipeline #
####################

# Examine JSON pipeline definition to ensure it is well formed
json.loads(pipeline.definition())

# Submit pipeline definition to SageMaker
pipeline.upsert(role_arn=role)

# Start pipeline execution
execution = pipeline.start()

# Describe the pipeline execution status
execution.describe()

# Wait for the pipeline to finish
execution.wait()

# Examine the report
evaluation_json = sagemaker.s3.S3Downloader.read_file("{}/evaluation.json".format(
    step_evaluation.arguments["ProcessingOutputConfig"]["Outputs"][0]["S3Output"]["S3Uri"]
))
json.loads(evaluation_json)

print("Wine Model Build Pipeline complete.")